﻿using System;

namespace week_1_exercise_6
{
    class Program
    {
        static void Main(string[] args)
        {
        /*
        "Hello World": 
        {
          "prefix": "helwrld",
          "body": 
          [
                "Console.Clear();",
                "Console.WriteLine(\"Hello World!\");"
          ],
          "description": "Print Hello World!"
        }
        */

            Console.Clear();
            Console.WriteLine("Hello World!");
        }
    }
}
